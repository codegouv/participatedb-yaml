ID: 12
Name: 'Online Deliberation: Design, Research, and Practice'
Description: '<p>Book edited by Todd Davies and Seeta Peña Gangadharan (<span class="caps">CSLI</span> Publications, November
  2009).</p>

  <p>From the book cover:</p>

  <blockquote>

  <p><strong>Can new technology enhance purpose-driven, democratic dialogue in groups, governments, and societies?</strong></p>

  </blockquote>

  <blockquote>

  <p><em>Online Deliberation: Design, Research, and Practice</em> is the ﬁrst book that attempts to sample the full range
  of work on online deliberation, forging new connections between academic research, technology designers, and practitioners.
  Since some of the most exciting innovations have occurred outside of traditional institutions, and those involved have often
  worked in relative isolation from each other, work in this growing ﬁeld has often failed to reﬂect the full set of perspectives
  on online deliberation. This volume is aimed at those working at the crossroads of information/communication technology
  and social science, and documents early ﬁndings in, and perspectives on, this new ﬁeld by many of its pioneers.</p>

  </blockquote>

  <p>From the book (page XV):</p>

  <blockquote>

  <p><strong>Preface</strong></p>

  </blockquote>

  <blockquote>

  <p>The present volume, Online Deliberation: Design, Research, and Practice, grew out of the Second Conference on Online
  Deliberation: Design, Research, and Practice (OD2005/<span class="caps">DIAC</span>-2005), which was held at Stanford University
  May 20-22, 2005. After the conference, participants were offered the opportunity to submit draft manuscripts for publication.
  Beth Simone Noveck assisted in the selection, and we secured an agreement with <span class="caps">CSLI</span> Publications
  to publish the book simultaneously in print and in a free version online. Seeta Gangadharan joined the project as a coeditor
  in 2008, and contributed a concluding chapter.</p>

  </blockquote>

  <blockquote>

  <p>In the age of the Internet, and especially in a field tied to evolving technology, it would be difficult to justify the
  time required to carefully edit a book whose purpose was to capture the latest technology. Instead, we sought to put together
  a collection that will have lasting value, capturing some of the most important lessons learned during the formative years
  of this field. The result, we hope, is a volume that will serve as a useful record and guide to the development of the field
  as we move forward in the years to come.</p>

  </blockquote>

  <blockquote>

  <p>There are several people we would like to thank: Beth Noveck for her help in the early stages of this project, Dikran
  Karagueuzian of <span class="caps">CSLI</span> Publications for his friendly helpfulness and patience, Robert Cavalier and
  Peter Shane for helping to organize the Stanford conference, Socoro Relova and Natalie Mendoza for providing staff support,
  Jim Fishkin for providing help and advice in organizing the conference and since, Fiorella De Cindio and Jerry Feldman for
  reviewing and vetting the manuscript, and all of the authors for sticking with this project through publication. We are
  most grateful.</p>

  </blockquote>

  <blockquote>

  <p>–Todd Davies and Seeta Peña Gangadharan, Stanford, August 29, 2009</p>

  </blockquote>

  <p>The book is licensed under a Creative Commons Attribution-Share Alike 3.0 Unported License. A <span class="caps">PDF</span>
  version is available for download.</p'
Added: December 10, 2009
Last updated: April  5, 2010
Web: http://odbook.stanford.edu
Country: United States of America
Related tools:
- deme
- e-liberate
- perlnomic
- picola-delibera-2-0
- picola-lite
- reason-able
